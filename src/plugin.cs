﻿/*
Copyright (C) 2023 Tulpenkiste

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using BepInEx;
using BepInEx.Logging;

using System;
using System.Collections;

using GameConsole;
using BepInEx.Configuration;

namespace UltraShellMommy {
	[BepInPlugin("UltraShellMommy", "Ultra Shell Mommy", "1.0.0")]
	[BepInProcess("ULTRAKILL.exe")]
    public class UltraShellMommy : BaseUnityPlugin {
		List<ICommand> Commands = new List<ICommand>();

		private ConfigEntry<String> CfgMommysLittle;
		private ConfigEntry<String> CfgMommysRole;
		private ConfigEntry<String> CfgMommysPronouns;
		private ConfigEntry<String> CfgMommysPositiveResponses;
		private ConfigEntry<String> CfgMommysNegativeResponses;

		public String[] MommysLittle;
		public String[] MommysRole;
		public String[] MommysPronouns;
		public String[] MommyPositiveResponses;
		public String[] MommyNegativeResponses;

		private void Awake() {
			Commands.Add(new ShellMommy(this));
			// Register a new command
			GameConsole.Console.Instance.RegisterCommand(Commands[0]);

			// This could be more efficient, but too bad! I am too lazy.
			CfgMommysLittle = Config.Bind("Mommy", "MommysLittle", "girl",
			"Sets the affectionate term that mommy will use to refer to the user. Note: this and every other configurable value is split by a '/'.");

			CfgMommysPronouns = Config.Bind("Mommy", "MommysPronouns", "her",
			"Sets the pronouns that mommy will use to refer to itself.");

			CfgMommysRole = Config.Bind("Mommy", "MommysRole", "mommy",
			"Sets the role that mommy will have.");

			CfgMommysPositiveResponses = Config.Bind("Mommy", "MommysPositiveResponses", "*pets your head*/awe, what a good AFFECTIONATE_TERM~\nMOMMYS_ROLE knew you could do it~ ❤️/good AFFECTIONATE_TERM~\nMOMMYS_ROLE's so proud of you~ ❤️/Keep up the good work, my love~ ❤️/MOMMYS_ROLE is proud of the progress you've made~ ❤️/MOMMYS_ROLE is so grateful to have you as MOMMYS_PRONOUN little AFFECTIONATE_TERM~ ❤️/I'm so proud of you, my love~ ❤️/MOMMYS_ROLE is so proud of you~ ❤️/MOMMYS_ROLE loves seeing MOMMYS_PRONOUN little AFFECTIONATE_TERM succeed~ ❤️/MOMMYS_ROLE thinks MOMMYS_PRONOUN little AFFECTIONATE_TERM earned a big hug~ ❤️/that's a good AFFECTIONATE_TERM~ ❤️/you did an amazing job, my dear~ ❤️/you're such a smart cookie~ ❤️",
			"Sets the possible negative responses that mommy will use. ");

			CfgMommysNegativeResponses = Config.Bind("Mommy", "MommysNegativeResponses", "do you need MOMMYS_ROLE's help~? ❤️/Don't give up, my love~ ❤️/Don't worry, MOMMYS_ROLE is here to help you~ ❤️/I believe in you, my sweet AFFECTIONATE_TERM~ ❤️/It's okay to make mistakes, my dear~ ❤️/just a little further, sweetie~ ❤️/Let's try again together, okay~? ❤️/MOMMYS_ROLE believes in you, and knows you can overcome this~ ❤️/MOMMYS_ROLE believes in you~ ❤️/MOMMYS_ROLE is always here for you, no matter what~ ❤️/MOMMYS_ROLE is here to help you through it~ ❤️/MOMMYS_ROLE is proud of you for trying, no matter what the outcome~ ❤️/MOMMYS_ROLE knows it's tough, but you can do it~ ❤️/MOMMYS_ROLE knows MOMMYS_PRONOUN little AFFECTIONATE_TERM can do better~ ❤️/MOMMYS_ROLE knows you can do it, even if it's tough~ ❤️/MOMMYS_ROLE knows you're feeling down, but you'll get through it~ ❤️/MOMMYS_ROLE knows you're trying your best~ ❤️/MOMMYS_ROLE loves you, and is here to support you~ ❤️/MOMMYS_ROLE still loves you no matter what~ ❤️/You're doing your best, and that's all that matters to MOMMYS_ROLE~ ❤️/MOMMYS_ROLE is always here to encourage you~ ❤️",
			"Sets the possible positive responses that mommy will use.");

			// Split everything into string[] by the '/' seperator
			MommysLittle = CfgMommysLittle.Value.Split('/');
			MommysPronouns = CfgMommysPronouns.Value.Split('/');
			MommysRole = CfgMommysRole.Value.Split('/');
			MommyPositiveResponses = CfgMommysPositiveResponses.Value.Split('/');
			MommyNegativeResponses = CfgMommysNegativeResponses.Value.Split('/');

			Logger.LogInfo("Loaded UltraShellMommy!");

			// License PrintLine
			GameConsole.Console.Instance.PrintLine("[UltraShellMommy] This ULTRAKILL BepInEx plugin is provided under the GNU General Public License v3.\nIf you did not receive such a file, you can obtain a copy at https://www.gnu.org/licenses/gpl-3.0.en.html.");
		}
	}
}
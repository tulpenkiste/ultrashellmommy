/*
Copyright (C) 2023 Tulpenkiste

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using GameConsole;
using System;

namespace UltraShellMommy {
	public class ShellMommy : ICommand {
		public ShellMommy(UltraShellMommy commandsMommy) {
			_mommy = commandsMommy;
		}

		public void Execute(GameConsole.Console con, string[] args) {
			if (args.Length < 1 || args[0] == "help") {
				// Print help and (if conditions are met) do ShellMommy negative response
				con.PrintLine("Usage: mommy <command> [args]");
				if (args.Length >= 1 && args[0] != "help") {
					DoResponse(con, false);
				}
			} else {
				// Check if command exists
				if (con.recognizedCommands.Keys.Contains(args[0])) {
					// Ok it exists, execute it
					var cmd = con.recognizedCommands[args[0]];
					List<String> newArgs = new List<String>();
					for (int i = 1; i < args.Length; i++) newArgs.Add(args[i]);
					cmd.Execute(con, newArgs.ToArray<string>());
					DoResponse(con, true);
				} else {
					UnityEngine.Debug.LogWarning("Unrecognized command: \"CMD\"".Replace("CMD", args[0]));
					DoResponse(con, false);
				}
			}
		}

		private void DoResponse(GameConsole.Console con, bool isPositive) {
			// Random number handler
			Random random = new Random();

			// Get random values from UltraShellMommy
			string mommyLittle = _mommy.MommysLittle[random.Next(_mommy.MommysLittle.Length)];
			string mommyPronoun = _mommy.MommysPronouns[random.Next(_mommy.MommysPronouns.Length)];
			string mommyRole = _mommy.MommysRole[random.Next(_mommy.MommysRole.Length)];
			string response;

			if (isPositive) response = _mommy.MommyPositiveResponses[random.Next(_mommy.MommyPositiveResponses.Length)];
			else response = _mommy.MommyNegativeResponses[random.Next(_mommy.MommyNegativeResponses.Length)];

			// Replace text in the response
			response = response.Replace("AFFECTIONATE_TERM", mommyLittle);
			response = response.Replace("MOMMYS_PRONOUN", mommyPronoun);
			response = response.Replace("MOMMYS_ROLE", mommyRole);

			if (isPositive)	con.PrintLine(response);
			else UnityEngine.Debug.LogWarning(response);
		}

		// Command details
		public string Name => "mommy";
		public string Description => "Emulates a nuturing and supportive figure for you on your debugging journeys";
		public string Command => "mommy";

		// Current UltraShellMommy instance
		private UltraShellMommy _mommy;
	}
}
# UltraShellMommy
The wonders of [shell-mommy](https://github.com/sudofox/shell-mommy), now in your ULTRAKILL console/shell.<br>
Why? Got bored lmfao.
## Usage
Anything in `{arguments}` depends on the command being used.<br>
`mommy <command> {arguments}`.
# Dependencies
- BepInEx
- ULTRAKILL (must be a legitimate copy from Steam)
# Installing
Place `UltraShellMommy.dll` in the `BepInEx/plugins` folder in your ULTRAKILL installation folder.
# Building
Create a file called UltraShellMommy.csproj.user and fill in the data from the template then place it next to UltraShellMommy.<br>
Then, run `dotnet build` in the directory with UltraShellMommy.csproj.